import React, {Component} from 'react';

class PhotoItem  extends Component{

    showDetailPhoto = (photo,idx) => {
        this.props.showDetail(photo,idx);
    }

    render() {
        const photo = this.props.item;
        const idx = this.props.idx;

        return(
            <div className="photo-item">
                <img src={"http://localhost:8080/picmania/"+photo.url} onClick={() => this.showDetailPhoto(photo,idx)} width={200} height={200}/>
            </div>
        )
    }
}

export default PhotoItem;
