import React, {Component} from 'react';
import PhotoItem from "./PhotoItem";
import ModalDetailImage from "../modals/ModalDetailImage";

class ListPhotos extends Component{

    constructor(props) {
        super(props);
        this.state = {
            openModal:false
        }
    }
    closeModal = () => {
        this.setState({openModal:false})
    }

    showDetail = (photo, idx) => {
        this.props.showDetail(photo,idx);
    }

    showPhotos = (photos) => {
        console.log(photos)
        return(
            photos.map((photo,idx) => {
                return(
                   <PhotoItem item={photo} idx={idx} showDetail={this.showDetail}/>
                )
            })
        )
    }

    render(){
        return(
            <div>
                <div  className="photos-content">
                    {this.showPhotos(this.props.photos)}
                </div>
            </div>
        )
    }
}


export default ListPhotos;
