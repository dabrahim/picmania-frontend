import React, {Component, } from "react";
import ModalCreateImage from "./modals/ModalCreateImage";
import ModalCreateAlbum from "./modals/ModalCreateAlbum";

class  MyHeader extends  Component{
    constructor(props) {
        super(props);
        this.state = {
            openModalImage:false,
            openModalAlbum:false,
        }
    }

    showModalImage = () =>{
        this.setState({openModalImage : true})
    }

    showModalAlbum = () =>{
    this.setState({openModalAlbum : true})
}
    closeModalImage = () => {
        this.setState({openModalImage:false});
    }

    closeModalAlbum = () => {
        this.setState({openModalAlbum:false})
    }

    render(){
        return(
            <div id="header">
                <ModalCreateImage open={this.state.openModalImage} closeModal={this.closeModalImage}/>
                <ModalCreateAlbum open={this.state.openModalAlbum} closeModal={this.closeModalAlbum}/>

                <img id="logo" src={require("../images/logo_white.png")} height="200px"/>
                <ul>
                    <li onClick={this.showModalAlbum}>Créer Albums</li>
                    <li onClick={this.showModalImage}>Uploader une Photo</li>
                </ul>
            </div>
        )
    }
}

export default MyHeader;
