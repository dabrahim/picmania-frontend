import React, {Component} from 'react';

import {InputField} from "../InputField";

import "../../css/FormStyle.css"

import API from '../../api';

import {useAuth} from "../../context/auth";

import {Redirect } from 'react-router-dom';

class FormLogin extends Component{

  constructor(props){
      super(props)
      this.state = {
        login: '',
        password: '',
        message:'',
      }
  }

  submitForm = (e) => {
    e.preventDefault();
    const formData = new FormData();

    formData.append('login',this.state.login);
    formData.append('password',this.state.password);

    API.post(`/api/users/login`,formData).then(response => {
        console.log(response);
        if(response.data.success){
            localStorage.setItem('token', response.data.payload.jwt);
            localStorage.setItem('id',response.data.payload.user.id);
            this.redirect('/');
        }else{
          this.setState({message:response.data.error.message})
        }
    }).catch((err) => {
      console.log(err);
      alert("Une erreur s'est produite au niveau du serveur.")
    })
  }

  setLogin = (loginInput) =>{
    this.setState({
      login:loginInput.target.value
    });
  }

  setPassword = (passwordInput) => {
    this.setState({
      password:passwordInput.target.value
    });
  }

  redirect(path='/') {
    if(useAuth()){
     return <Redirect to={path} />
    }
  }

  render(){

    return(
      <div>
        {this.redirect('/photos')}
        <span>  {this.state.message}</span>
        <form  onSubmit={this.submitForm}>
          <InputField label="Login" placeholder="Login"call={this.setLogin}/>
          <InputField label="Password" placeholder="Mot de Passe" call={this.setPassword}/>
          <button className="login-btn" type="submit">Se Connecter</button>
        </form>
      </div>
    )
  }
}

export default FormLogin;
