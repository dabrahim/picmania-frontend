import React, {Component} from "react";

import {InputField} from "../InputField";
import API from "../../api";

class FormCreatePhoto extends Component{
    constructor(props) {
        super(props);
        this.state = {
            image:{
                title:'',
                description:'',
                keywords: '',
                file:null,
                idAlbum:null,
            },

        }
    }

    setTitle = (input) =>{
        let _img = this.state.image;

        _img.title = input.target.value;

        this.setState({image:_img});
    }

    setDesc = (input) => {
        let _img = this.state.image;

        _img.description = input.target.value;

        this.setState({image:_img})
    }

    setFile = (input) => {
        var _img = this.state.image;
        _img.file = input.target.files[0];
        console.log(_img.file)
        this.setState({image:_img})
    }

    setKeywords = (input) => {
        let _img = this.state.image;
        _img.keyword = input.target.value;
        this.setState({image:_img});
    }

    setIdAlbum = (input) => {
        let _img = this.state.image;
        _img.idAlbum = input.target.value;
        this.setState({image:_img});
    }

    displayAlbums = (   ) =>{
        let albums = JSON.parse(localStorage.getItem('albums'))
        return(
            albums.map(album => {
                return(
                    <option value={album.id}>{album.name}</option>
                )
            })
        )
    }

    submitPhoto = (event) => {
        event.preventDefault();
        var formData = new FormData();
        let img = this.state.image;

        formData.append('title',img.title);
        formData.append('description', img.description);
        formData.append('image', img.file);

        API.post(`/api/albums/${img.idAlbum}/images`,formData).then(response => {
            if(response.data.success){
                let albums = JSON.parse(localStorage.getItem('albums'));
                albums.forEach(album => {
                    if(album.id == img.idAlbum){
                        album.images.push(response.data.payload);
                    }
                });

                localStorage.setItem('albums',JSON.stringify(albums))
                window.location.reload(true)
            }
            console.log(response)
        }).catch(error => {
            console.log(error)
        })
    }
    render(){
        return(
                <form onSubmit={this.submitPhoto}>
                    <div className="fields">
                        <InputField label="Titre" placeholder="Titre" call={this.setTitle}/>
                        <InputField label="Description" placeholder="Description Image" call={this.setDesc}/>
                    </div>
                    <div className="fields">
                        <InputField label="Keywords" placeholder="Mots clés" call={this.setKeywords}/>
                        <InputField type="file" call={this.setFile}/>
                    </div>
                    <div>
                      <select onChange={this.setIdAlbum}>
                        <option selected>Choisir un Album</option>
                          {this.displayAlbums()}
                      </select>
                    </div>
                    <button type="submit">Ajouter</button>
                </form>
             )
    }
}

export default FormCreatePhoto;
