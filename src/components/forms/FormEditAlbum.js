import React, {Component} from "react";
import API from "../../api";
import {InputField, TextAreaField} from "../InputField";

class FormEditAlbum extends Component{

    constructor(props) {
        super(props);
        console.log(this.props.album.isPublic)
        this.state = {
            name:this.props.album.name,
            is_public:this.props.album.isPublic,
            description:this.props.album.description
        }
        console.log(this.state.is_public)
    }

    setStatus = (input) => {
        this.setState({is_public:input.target.value})
        console.log(this.state.is_public)
    }

    setName = (input) => {
        this.setState({name:input.target.value})
    }

    setDescription = (input) => {
        this.setState({description:input.target.value})
    }

    editAlbum = (e) =>{
        e.preventDefault();

        var formData = new FormData();

        formData.append('name',this.state.name);
        formData.append('isPublic',this.state.is_public);
        formData.append('description',this.state.description);
        formData.append('userId',localStorage.getItem('id'))

        API.post(`/api/albums/${this.props.album.id}/update`,formData).then(response => {
            console.log(response)
            if(response.data.success){
                alert(response.data.message)
                let albums = JSON.parse(localStorage.getItem('albums'));
                albums.forEach((album,index) => {
                    if(album.id == this.props.album.id){
                        albums[index] = response.data.payload;
                    }
                })

                localStorage.setItem('albums',JSON.stringify(albums))

            }else{
                alert(response.data.message)
            }
        }).catch(error => {
            console.log(error)
        })
    }

    render(){
        return(
            <form onSubmit={this.editAlbum}>
                <InputField placeholder="Nom de l'album" value={this.state.name} call={this.setName}/>
                <div className="fields-radio">
                    <label>Public :</label>
                    <div className="item-radio">
                        <label htmlFor="">Non</label>
                        <input type="radio" value={!this.state.is_public}  checked={this.state.is_public} name="visibled" onChange={this.setStatus}/>
                    </div>
                    <div className="item-radio">
                        <label htmlFor="">Oui</label>
                        <input type="radio" value={this.state.is_public} checked={this.state.is_public} name="visibled" onChange={this.setStatus}/>
                    </div>
                </div>
                <TextAreaField placeholder="Description de l'album" value={this.state.description} call={this.setDescription}/>
                <button type="submit">Modifier</button>
            </form>
        )
    }
}

export default FormEditAlbum;
