import React, {Component} from  'react';

import {InputField, TextAreaField} from "../InputField";

import API from '../../api';

class FormCreateAlbum  extends Component{
    constructor(props) {
        super(props);
        this.state = {
            name:'',
            is_public:true,
            description:''
        }
    }

    setStatus = (input) => {
        this.setState({is_public:input.target.value})
    }

    setName = (input) => {
      this.setState({name:input.target.value})
    }

    setDescription = (input) => {
      this.setState({description:input.target.value})
    }

    createAlbum = (e) =>{
      e.preventDefault();

      var formData = new FormData();

      formData.append('name',this.state.name);
      formData.append('isPublic',this.state.is_public);
      formData.append('description',this.state.description);
      formData.append('userId',localStorage.getItem('id'))

      API.post(`/api/albums/`,formData).then(response => {
        if(response.data.success){
          alert(response.data.message)
        }else{
          alert(response.data.message)
        }
      }).catch(error => {
        console.log(error)
      })
    }

    render(){
        return(
            <form onSubmit={this.createAlbum}>
                <InputField placeholder="Nom de l'album" call={this.setName}/>
                <div className="fields-radio">
                    <label>Public :</label>
                    <div className="item-radio">
                        <label htmlFor="">Non</label>
                        <input type="radio" value={!this.state.is_public} name="visibled" onChange={this.setStatus}/>
                    </div>
                    <div className="item-radio">
                        <label htmlFor="">Oui</label>
                        <input type="radio" value={this.state.is_public} name="visibled" onChange={this.setStatus}/>
                    </div>
                </div>
                <TextAreaField placeholder="Description de l'album" call={this.setDescription}/>
                <button type="submit">Créer</button>
            </form>
        )
    }
}

export default FormCreateAlbum;
