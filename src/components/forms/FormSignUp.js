import React, {Component} from 'react';
import {InputField} from "../InputField";
import API from '../../api';
import {Redirect} from 'react-router-dom';

class FormSignUp extends Component{
    constructor() {
        super();
        this.state = {
          user:{
            lastName:'',
            firstName : '',
            login: '',
            password:'',
            avatar:null
          },
          message:'',
          redirect:false,
      }
    }

    setNom = (input) => {
        var us = this.state.user;
        us.lastName = input.target.value;
        this.setState({user:us})
    }

    setPrenom = (input) => {
        var us = this.state.user;
        us.firstName = input.target.value;
        this.setState({user:us})
    }

    setLogin = (input) => {
      var us = this.state.user;
      us.login = input.target.value;
      this.setState({user:us})
    }

    setPassword = (input) => {
      var us = this.state.user;
      us.password = input.target.value;
      this.setState({user:us})
    }

    myFile = (input) => {
      var user = this.state.user;
      user.avatar = input.target.files[0];
      this.setState({user:user})
    }

    redirectUser = () => {
      if (this.state.redirect) {
        return <Redirect to="/login" />
      }
    }

    submit = (event) => {
      event.preventDefault();
      const formData = new FormData();
      const user  = this.state.user;

      formData.append('avatar',user.avatar)
      formData.append('lastName',user.lastName);
      formData.append('firstName',user.firstName);
      formData.append('password',user.password);
      formData.append('login',user.login);



      API.post(`/api/users`, formData,{
        headers: {
          "Content-Type": "multipart/form-data",
          "Access-Control-Allow-Origin": "*"
        }}
      ).then(res => {
        console.log(res)
        if(res.data.success){
          this.setState({message:res.data.message})
          this.setState({redirect:true});
        }else{
          this.setState({message:res.data.message})
        }

      }).catch((error) => {console.log(error)})

    }

    render() {
        return(
            <div>
                {this.redirectUser()}
                <span>{this.state.message}</span>
                <form onSubmit={this.submit}>
                    <InputField label="Nom" placeholder="Nom" call={this.setNom}/>
                    <InputField label="Prénom" placeholder="Prénom" call={this.setPrenom}/>
                    <InputField label="Login" placeholder="Login" call={this.setLogin}/>
                    <InputField label="Password" placeholder="Password" call={this.setPassword}/>
                    <input type="file" name="file" onChange={this.myFile}/>
                    <button className="btn_black" type="submit">Créer</button>
                </form>
            </div>
        )
    }
}

export default FormSignUp;
