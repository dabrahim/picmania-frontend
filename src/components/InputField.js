import React, {Component} from 'react';
import "../css/style.css";

class InputField extends Component{
    constructor() {
        super();
        this.handler = this.handler.bind(this);
    }

    handler(input){
        this.props.call(input)
    }

    render() {
        return(
            <div className="inputField">
                <input type={this.props.type} placeholder={this.props.placeholder} value={this.props.value} onChange={input => this.handler(input)}/>
            </div>
        )
    }
}


class TextAreaField extends Component{
    constructor(props) {
        super(props);
        this.handler = this.handler.bind(this);
    }

    handler(input){
        this.props.call(input);
    }

    render(){
        return(
            <div>
                <textarea placeholder={this.props.placeholder} cols="30" rows="5" onChange={input => this.handler(input)}>{this.props.value}</textarea>
            </div>
        )
    }
}

export {TextAreaField,  InputField};

