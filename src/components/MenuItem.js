import React, {Component} from "react";
import {img_album_svg, img_photo_svg, img_settings_svg} from "./Icons";


class MenuItem extends Component{
    constructor(props) {
        super(props);
        this.state = {
            menuPhotoSelected:false,
            menuAlbumSelected:false,
            menuSettingSelected:false,
        }


        console.log(this.props.selected)
    }

    componentWillMount() {
        switch (this.props.selected) {
            case "photos":
                this.setState({menuPhotoSelected:true});
                break;
            case "albums":
                this.setState({menuAlbumSelected:true});
                break;
            case "settings":
                this.setState({menuSettingSelected:true});
                break;
        }
    }

    render(){
        return(
            <div>
                <a className={`menu-item menu-item-photo ${this.state.menuPhotoSelected?"selected-item-photo":""}`}  href="/photos">
                    {img_photo_svg()}
                    <span>Photos</span>
                </a>
                <a  className={`menu-item menu-item-album ${this.state.menuAlbumSelected?"selected-item-album":""}`} href="/albums">
                    {img_album_svg()}
                    <span>Albums</span>
                </a>
                <a className={`menu-item menu-item-settings ${this.state.menuSettingSelected?"selected-item-settings":""}`} href="/settings">
                    {img_settings_svg()}
                    <span>Settings</span>
                </a>
            </div>
        )
    }
}


export default MenuItem;
