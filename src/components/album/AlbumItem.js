import React, {Component} from 'react';

import {Redirect} from "react-router-dom"

class AlbumItem extends Component{


  render(){
    const album = this.props.item;
    return(
        <div className="album-item" onClick={() =>window.location=`/album/${album.name}` }>
          <img src="blank.svg"/>
          <div className="album-item-libelle">
            <h4>{album.name}</h4>
            <small><i>Créé le 6 Frévrier 2020</i></small>
          </div>
        </div>
    )
  }
}


export default  AlbumItem;
