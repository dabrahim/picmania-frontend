import React, {Component} from 'react';
import AlbumItem from "./AlbumItem";

class ListAlbums extends Component{
  constructor(props) {
    super(props);

  }

  showAlbums = (albums) => {
    return(
        albums.map(album => {
          return(
              <AlbumItem
                item={album}
                key={album.id}
              />
          )
        })
    )
  }

  render(){
    return(
        <div>
            <h2>Mes Albums</h2>
            <div className="albums-content">
                {this.showAlbums(this.props.albums)}
            </div>
        </div>
    )
  };
}


export default ListAlbums;
