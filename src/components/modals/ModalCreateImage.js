import React, {Component} from "react";
import HyperModal from "react-hyper-modal";
import "../../css/modalCreateStyle.css"
import FormCreatePhoto from "../forms/FormCreatePhoto";


class ModalCreateImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible:false,
        }
    }

    render() {
        return (
            <HyperModal
                isOpen={this.props.open}
                requestClose={this.props.closeModal}
            >
                <div id="modal-form-content">
                    <h1 id="modal-form-title">Ajouter une Nouvelle Photo</h1>
                    <div id="modal-form-details">
                        <FormCreatePhoto/>
                    </div>
                </div>
            </HyperModal>
        );
    }
}

export default ModalCreateImage;
