import React, {Component} from "react";
import HyperModal from "react-hyper-modal";
import "../../css/modalStyle.css"

class ModalDetailImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible:true,
        }
    }

    showDetailImage = () => {
        let status = this.state.visible;
        this.setState({visible:!status})
    }
    render() {
        const photo = this.props.photo;
        console.log(photo)
        return (
            <HyperModal
                isOpen={this.props.open}
                requestClose={this.props.closeModal}
            >
                <div id="modal-content">
                    <h1 id="modal-title">Titre de notre Message</h1>
                    <div id="modal-details">
                        <div id="image-content">
                            <img src={"http://localhost:8080/picmania"+photo.url+""} />
                        </div>
                        <div>
                            <h3>Description :</h3>
                            <p>{photo.description}</p><br/>
                            <a href=""></a>
                        </div>

                    </div>
                </div>
            </HyperModal>
        );
    }
}

export default ModalDetailImage;
