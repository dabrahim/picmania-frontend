import React, {Component} from "react";

import HyperModal from "react-hyper-modal";

import "../../css/modalCreateStyle.css";

import FormEditAlbum from "../forms/FormEditAlbum";

class ModalEditAlbum extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible:false,
        }
    }

    render() {
        return (
            <HyperModal
                isOpen={this.props.open}
                requestClose={this.props.closeModal}
            >
                <div id="modal-form-content">
                    <h1 id="modal-form-title">Modifier un Album</h1>
                    <div id="modal-form-details">
                        <FormEditAlbum album={this.props.album}/>
                    </div>
                </div>
            </HyperModal>
        );
    }
}

export default ModalEditAlbum;
