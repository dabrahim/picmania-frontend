import { createContext, useContext } from 'react';

export const AuthContext = createContext();

export function useAuth() {
    const token = localStorage.getItem("token");

    if(token){
        return true;
    }else{
        return false;
    }
}
