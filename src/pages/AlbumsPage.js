import React, {Component} from 'react';

import MyHeader from "../components/MyHeader";
import MenuItem from "../components/MenuItem";
import API from "../api";
import ListAlbums from "../components/album/ListAlbums";

class AlbumsPage extends Component{
    constructor() {
        super();
        this.state= {
            albums:[],
        }
    }

    getAlbums = () => {
        let userId = localStorage.getItem('id');
        API.get(`/api/users/${userId}/albums/`).then(response => {
            console.log(response)
            this.setState({albums:response.data});
           localStorage.setItem('albums',JSON.stringify(response.data));
        }).catch(error => {
            console.log(error);
        })
    }

    componentDidMount() {
        this.getAlbums();
    }

    render() {

        return(
            <div id="container">

                <MyHeader/>
                <div id="content">
                    <div className="right-panel">
                        <MenuItem selected="albums"/>
                    </div>
                    <div className="left-panel">
                      <ListAlbums albums={this.state.albums}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default  AlbumsPage;
