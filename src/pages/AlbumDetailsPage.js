import React, {Component} from 'react';
import MyHeader from "../components/MyHeader";
import MenuItem from "../components/MenuItem";
import ListPhotos from "../components/photo/ListPhotos";
import ModalEditAlbum from "../components/modals/ModalEditAlbum";

class AlbumDetailsPage  extends Component{
    constructor(props) {
        super(props);
        console.log(props)
        this.album= []
        this.state = {
            open:false
        }

    }

    showModal = () =>{
        this.setState({open : true})
    }

    close = () => {
        this.setState({open:false});
    }

    displayAlbum = () => {
        return(
            <ListPhotos showDetail={this.showDetailPhoto} photos={this.album.images}/>
        )
    }

    getCurrentAlbum = () => {
        let match = this.props.match.params.name;
        this.album = JSON.parse(localStorage.getItem('albums')).filter(album => album.name === match)[0]
    }

    componentWillMount() {
        this.getCurrentAlbum();
    }

    render(){
        return(
            <div id="container">
                <ModalEditAlbum open={this.state.open} closeModal={this.close} album={this.album}/>
                <MyHeader/>
                <div id="content">
                    <div className="right-panel">
                        <MenuItem selected="albums"/>
                    </div>
                    <div className="left-panel">
                        <h2>Album : {this.album.name}</h2><br/><button onClick={this.showModal}>Modifier</button>
                        {this.displayAlbum()}
                    </div>
                </div>
            </div>
        )
    }
}


export default AlbumDetailsPage;
