import React,{Component} from 'react';

import MyHeader from "../components/MyHeader";
import MenuItem from "../components/MenuItem";
import "../css/dashboard-style.css"

class SettingsPage extends Component{

    constructor(props) {
        super(props);
    }

    render(){
        return(
            <div id="container">
                <MyHeader/>
                <div id="content">
                    <div className="right-panel">
                        <MenuItem selected="settings"/>
                    </div>
                    <div className="left-panel">
                        <h2>Paramétres</h2>
                        <div className="settings-content">

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default SettingsPage;
