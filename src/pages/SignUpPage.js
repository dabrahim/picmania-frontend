import React , {Component} from 'react';
import FormSignUp from "../components/forms/FormSignUp";

class SignUpPAge extends Component{

    render(){
        return(
                <div className="container">
                    <div className="form-left-side">
                        <img id="logo" src={require("../images/logo_white.png")} alt=""  />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat molestias quia ratione sed unde! Amet aperiam </p>
                    </div>
                    <div className="form-content">
                        <p>
                            <h2>Inscription</h2>
                            <small>J'ai déja un compte. <a href="/login">Se connecter</a></small>
                        </p>
                        <FormSignUp/>
                    </div>
                </div>
        )
    }
}


export default SignUpPAge;
