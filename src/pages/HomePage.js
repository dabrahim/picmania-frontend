import React, {Component} from 'react';
import Gallery from 'react-photo-gallery';
import "../css/homePage-style.css"
import {useAuth} from "../context/auth";
import API from "../api";

class HomePage extends Component {
  constructor() {
    super();
    this.state ={
       photos :[],
    }
  }

  displatGallery = () => {
      let p = this.state.photos;
      let po = [];

      p.forEach(photo => {
          let i = {
              src:"http://localhost:8080/picmania"+photo.url,
              width:4,
              height:3
          }
          po.push(i);
      })

    return(
        <Gallery photos={po} />
    )
  }

  getAlbums = () => {
    API.get(`/api/albums`).then(response => {
      let albums = response.data;

      let _photos = [];

      albums.forEach(album => {
        _photos.push(...album.images)
      })

      this.setState({photos:_photos});
    }).catch(error => {
      console.log(error)
    })
  }

  componentDidMount() {
    this.getAlbums();
  }

  render(){

    return(
        <div id="container-home-page">
          <div id="header-home-page">
            <img id="logo" src={require("../images/logo_white.png")} height="200px"/>
            <div id="home-page-menu">
            { useAuth() ? <a href="logout" className="text-white">Se Déconnecter</a>:
                <a href="login" className="text-white">Se Connecter</a>}
              <a href="/signup" className="text-white">Créer un compte</a>
            </div>
          </div>
          <div id="home-page-content">
            <div id="header-image">
                <h2>Beinvenue les enfants</h2>
            </div>
            <div id="photos">
              {this.displatGallery()}
            </div>

          </div>

        </div>

    )
  }
}

export default HomePage;
