import React ,{Component} from 'react'
import "../css/dashboard-style.css";
import MenuItem from "../components/MenuItem";
import PhotosPage from "./PhotosPage";

const header = () => {
    return(
        <div id="header">
            <img id="logo" src={require("../images/logo_white.png")} height="200px"/>
            <ul>
                <li>Créer Albums</li>
                <li>Ajouter une nouvelle Photo</li>
            </ul>
        </div>
    )
}
class Dashboard extends Component{
  constructor() {
    super();
  }

  render() {
    return(
      <div id="container">
          {header()}
        <div id="content">
          <div className="right-panel">
            <MenuItem/>
          </div>
          <PhotosPage/>
        </div>
      </div>
    )
  }
}


export default Dashboard;
