import React, {Component} from 'react';

import FormLogin from '../components/forms/FormLogin';
import Particles from 'react-particles-js';
import "../css/style.css";

class LoginPage extends Component {

  render(){
    return(
        <div id="container" className="back">
            <div id="simple-header-bar">
                <img id="logo" src={require("../images/logo.png")}/>
            </div>
            <Particles  params={{
                "particles": {
                    "number": {
                        "value": 150
                    },
                    "size": {
                        "value": 5
                    }
                },
                "interactivity": {
                    "events": {
                        "onhover": {
                            "enable": true,
                            "mode": "repulse"
                        }
                    }
                }
            }}/>
            <div id="form-login">
                <div>
                    <h1>Connexion</h1>
                    <small>j'ai pas de compte. <a href="/signUp">Créer un compte</a>.</small><br/>
                </div><br/>
                <FormLogin/>

            </div>
            <div >

            </div>
        </div>
    )
  }
}

export default LoginPage;
