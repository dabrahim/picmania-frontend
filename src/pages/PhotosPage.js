import React, {Component} from 'react';
import MyHeader from "../components/MyHeader";
import MenuItem from "../components/MenuItem";
import ListPhotos from "../components/photo/ListPhotos";
import "../css/dashboard-style.css";
import ModalDetailImage from "../components/modals/ModalDetailImage";
import ReactNotification from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'


class PhotosPage extends Component{
    constructor() {
        super();
        this.state= {
            open:false,
            photos : [],
            selectedPhoto : {url:''},
        }
    }


    openModal = () => {
        this.setState({open:true,message:"Salut les zeros"})
    }

    closeModal = () => {
        this.setState({open:false});
    }

    showDetailPhoto = (photo, idx) => {
        this.setState({selectedPhoto:photo})
        this.setState({open:true})

    }

    displayPhoto = ()=> {
        let albums = JSON.parse(localStorage.getItem('albums'));

        let _photos = [];
        albums.forEach(album => {
            if(album.images.length > 0){
                _photos.push(...album.images)
            }
        })

        this.setState({photos:_photos});
    }

    componentDidMount() {
        this.displayPhoto();
    }

    render() {
        return(
            <div id="container">
                <ModalDetailImage open={this.state.open} photo={this.state.selectedPhoto} closeModal={this.closeModal}/>
                <MyHeader showModalUploadPhoto={this.showModal} />
                <div id="content">

                    <div className="right-panel">
                        <MenuItem selected="photos"/>
                    </div>
                    <div className="left-panel">
                        <h2>Mes Photos</h2>
                        <ListPhotos showDetail={this.showDetailPhoto} photos={this.state.photos}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default PhotosPage;
