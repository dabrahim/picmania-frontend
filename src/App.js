import React, {useState} from 'react';

import './App.css';

import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import SignUpPage from './pages/SignUpPage';
import PhotosPage from "./pages/PhotosPage";
import AlbumsPage from "./pages/AlbumsPage";
import AlbumDetailsPage from "./pages/AlbumDetailsPage";
import SettingsPage from "./pages/SettingsPage";

import PrivateRoute from "./PrivateRoute";

import  {Switch,Route, BrowserRouter as Router} from 'react-router-dom';

function App() {
    return (
        <Router >
            <Switch>
                <Route path="/" exact component={HomePage}/>
                <Route path="/login"  component={LoginPage}/>
                <Route path="/signup"  component={SignUpPage}/>
                <PrivateRoute path="/photos"  component={PhotosPage}/>
                <PrivateRoute path="/albums"  component={AlbumsPage}/>
                <PrivateRoute path="/album/:name"  component={AlbumDetailsPage}/>
                <PrivateRoute path="/settings"  component={SettingsPage}/>
            </Switch>
        </Router>
  );
}

export default App;
